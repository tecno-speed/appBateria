document.addEventListener('deviceready', onDeviceready)

function onDeviceready() {
    window.addEventListener('batterystatus', onBatteryStatus)
}

function onBatteryStatus(status) {
    document.getElementById('status').innerHTML = "Level: " + status.level + " Conectado: " + status.isPlugged 
}